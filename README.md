---
title: README du projet portail
date: 2022-11-18T09-20-23+01:00
id: 20221118T092037
tags: [cmu, portail web, documentation]
---

## Table des matières

* [Description](#description)
* [Les blocs HTML](#les-blocs-html)
* [Titres](#titres)
* [Références bibliographiques](#références-bibliographiques)
  * [Liste à puce sans puce](#liste-à-puce-sans-puce)
  * [Exposer les données bibliographiques pour Zotero](#exposer-les-données-bibliographiques-pour-zotero)
* [Ancres dans les `expand/collapse`](#ancres-dans-les-expand-collapse)

## Description

Ce projet a pour objectif de documenter mes méthodes d'édition du portail de
médecine clinique du site de la bibliothèque de l'UNIGE. Il contient également
des fichiers HTML lorsque je crée un bloc HTML dans concrete5 plutôt qu'un bloc
de type contenu.

## Les blocs HTML

Les blocs contenus, s'ils facilitent la vie avec leur éditeur WYSIWYG, sont
toutefois limités pour réaliser des structures HTML de meilleure qualité et
surtout permettent moins d'améliorer le rendu, puisqu'il faut utiliser les
classes disponibles.

Le bloc HTML oblige d'écrire soi-même le HTML, mais à l'inverse, il permet
d'écrire soi-même le HTML, et ainsi d'avoir plus de liberté, que ce soit pour
maîtriser les balises utilisée et améliorer ainsi la sémantique, ou pour
affiner la mise en forme grâce à l'inclusion de règles CSS dans la balise
`<style>`.

Pour ma part, je procède ainsi :

1. Je rédige le HTML que je désire dans un éditeur de texte, si possible qui
   soit en mesure d'identifier les erreurs de syntaxe.
1. J'utilise une balise `<article>` qui contient l'ensemble du code nécessaire,
   auquel j'ajoute une classe du genre `mc-transversales-bd`, à savoir les
   initiales de la discipline, le nom de la page, le nom de la section (du
   bloc). C'est aussi dans cette balise que je place la balise `<style>`
   contenant les règles CSS, du moins si c'est nécessaire.
1. J'ajoute un nouveau bloc, un bloc HTML.
1. Je copie-colle mon code HTML et j'enregistre.

```html
<article class="mc-transversales-bd">
<!-- Le code HTML viendra ici -->
<style>
// Les règles CSS viennent ici
.mc-transversales-bd {
  fonte-size: large;
}
</style>
</article>
```

Les fichiers `*.html` de ce dépôt sont des blocs entièrement créés en HTML. Il
peuvent contenir plus de balises que nécessaires, afin de pouvoir les afficher
de manière indépendante au site de l'UNIGE.

C'est le cas du bloc `ebooks-series.html` par exemple. Dans ces cas-là, il ne
faut copier que le contenu, le plus souvent dans une balise `article` avec une
classe dont le nom correspond au bloc (`mc-es-main`, pour médecine clinique,
ebooks-series, section principale).

## Titres

Les titres doivent contenir les classes `o-title--hx` où x correspond au niveau
de titre.

```html
<h2 class="o-title--h2">Titre</h2>
```

S'il s'agit d'un titre dans un bloc de propriété *Box - Titre - 2017*, alors il
doit également contenir la classe `c-NewsWall-itemTitle`.

```html
<h2 class="o-title--h2 c-NewsWall-itemTitle">Titre</h2>
```

## Références bibliographiques

### Liste à puce sans puce

Pour afficher une référence bibliographique sur plusieurs lignes, on peut
utiliser la liste non ordonnée. Par contre, il faut annuler la règle de style
qui affiche des puces d'une manière un peu étonnante.

```html
<article class="mc-dic-item">
    <ul>
      <li>Marroun I. <em>Le nouveau dictionnaire médical</em>. 7<sup>e</sup> éd.</li>
      <li>Issy-les-Moulineaux : Elsevier Masson, 2018.</li>
      <li>Cote : REF 1 W 13 14 ed 7</li>
      <li>
        <a href="https://www.elsevierelibrary.fr/articledfreader/nouveau-dictionnaire-mdical"
           lang="en"
           hreflang="English"
           ref="nofollow"
           target="_blank">
          Livre électronique en ligne
        </a>.
      </li>
      <li>
        <a lang="fr"
           dir="ltr"
           type="html/text"
           href="https://slsp-unige.primo.exlibrisgroup.com/articleermalink/41SLSP_UGE/l0hvjc/alma991008098789705502"
           hreflang="français"
           rel="nofollow"
           target="_blank">
          Le document dans le catalogue Swisscovery
        </a>.
      </li>
    </ul>
    <span class='Z3988' title='url_ver=Z39.88-2004&amp;ctx_ver=Z39.88-2004&amp;rfr_id=info%3Asid%2Fzotero.org%3A2&amp;rft_id=urn%3Aisbn%3A978-2-294-74357-3&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Abook&amp;rft.genre=book&amp;rft.btitle=Le%20nouveau%20dictionnaire%20m%C3%A9dical&amp;rft.place=Issy-les-Moulineaux&amp;rft.publisher=Elsevier%20Masson&amp;rft.edition=7e&amp;rft.aufirst=Ibrahim&amp;rft.aulast=Marroun&amp;rft.au=Ibrahim%20Marroun&amp;rft.au=Thomas%20Sen%C3%A9&amp;rft.au=Jacques%20Quevauvilliers&amp;rft.au=Abe%20Fingerhut&amp;rft.date=2018&amp;rft.isbn=978-2-294-74357-3&amp;rft.language=fre%3Beng'></span>
  </article>
<style>
.mc-dic-item > ul > li {
  padding: 0 !important;
}
.mc-dic-item li:not([class])::before,
.mc-dic-item li::before {
  content: none;
}
</style>
```

La première règle permet de supprimer le *padding*, alors que la deuxième
« vide » le contenu du `::before` contenant la puce sous forme de police
d'écriture (fontawesome).

### Exposer les données bibliographiques pour Zotero

Une solution simple est l'ajout de *microdata* directement dans le HTML grâce
aux [*COinS*][coins]. Et pour se faciliter la vie, il faut utiliser Zotero pour
générer les données.

**Ne fonctionne qu'au sein d'un bloc HTML, sinon concrete5 va effacer le
`<span>`, car il est vide**, puisque tout est dans la partie attribut de la
balise.

1. Ajouter les références nécessaires dans son Zotero.
1. Clic-droit sur la ou les références et « Exporter le document… ».
1. Choisir le format « COinS ».
1. Enregistrer le fichier sur son disque dur, l'extension de fichier `.html`
   facilite un peu les choses.
1. Copier le code obtenu (`<span class…></span>`) dans le contenu HTML du bloc.

[L'exemple ci-dessus][liste] montre justement un exemple de référence
bibliographique content les métadonnées `COinS` reconnues par Zotero.


[coins]: https://www.zotero.org/support/dev/exposing_metadata/coins
[liste]: #liste-à-puce-sans-puce

## Ancres dans les `expand/collapse`

Afin de pouvoir utiliser un ancre qui puisse ouvrir un composant qui s'ouvre ou
se ferme (bloc `expand/collapse`), il faut :

1. Insérer l'ancre au sein du contenu du bloc.
1. Modifier les propriétés de la page :
    1. Menu *Éditer/Édition*, puis *propriétés*.
    1. Onglet *Attributs personnalisés*.
    1. *Contenu d'entête supplémentaire*.
    1. Coller le contenu suivant :

```html
<script type="text/javascript">
function getAnchor() {
	return (document.URL.split('#').length > 1) ? document.URL.split('#')[1] : null;
}

function getOffsetTop(elem){
	var offsetTop = 0;
	do {
		if(!isNaN(elem.offsetTop)){
			offsetTop += elem.offsetTop;
		}
	} while( elem = elem.offsetParent );
	return offsetTop;
}

function getOffsetLeft(elem){
	var offsetLeft = 0;
	do {
		if(!isNaN(elem.offsetLeft)){
			offsetLeft += elem.offsetLeft;
		}
	} while( elem = elem.offsetParent );
	return offsetLeft;
}


window.addEventListener("load",function(){
	var monanchor = getAnchor();
	if (typeof monanchor !== 'undefined' && monanchor !== null) {
		substring = "-title-";
		if (monanchor.indexOf(substring) !== -1) {
			monanchor = monanchor.replace("-title-","-content-");
			var parentDiv = document.getElementById(monanchor);
		} else {
			var pDoc = document.getElementsByName(monanchor)[0];
			var parentDiv = pDoc.parentNode.parentNode;
		}
		parentDiv.style.display = "block";
		var offsetTop = getOffsetTop(parentDiv);
		var offsetLeft = getOffsetLeft(parentDiv);
		window.scrollTo(offsetLeft, offsetTop - 200);
		}
	},false);
</script>
```

## Blocs créés en HTML

Les exemples ci-dessous sont des blocs entièrement créés en HTML. Il peuvent
contenir plus de balises que nécessaires, afin de pouvoir les afficher de
manière indépendante au site de l'UNIGE.

C'est le cas du bloc `ebooks-series.html` par exemple. Dans ces cas-là, il ne
faut copier que le contenu, le plus souvent dans une balise `article` avec une
classe dont le nom correspond au bloc (`mc-es-main`, pour médecine clinique,
ebooks-series, section principale).
